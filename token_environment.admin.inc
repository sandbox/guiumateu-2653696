<?php

/**
 * @file
 * Page callbacks and admin forms of the token_environment module.
 */

/**
 * Callback for the token_environment page.
 *
 * @return string
 *   The token environment page HTML.
 */
function token_environment_page() {
  // Get environment.
  $env = token_environment_get_env();

  // Create table.
  $variables['header'] = array(
    t('Environment'),
    t('Use it in body class'),
  );

  if (!empty($env)) {
    $variables['rows'][] = array(
      $env,
      variable_get('token_environment_body_class', NULL) ? 'Yes' : 'No',
    );
  }

  $variables['empty'] = t('The environment has not defined.');

  return theme('table', $variables);
}


/**
 * The add/edit token environment form.
 *
 * @param array $form
 *   The token type edit form.
 * @param array $form_state
 *   The form state.
 *
 * @return array
 *   The complete form array.
 */
function token_environment_edit_form(array $form, array &$form_state) {
  $form = array();

  // Get environment.
  $env = token_environment_get_env();

  // Create form.
  $form['environment'] = array(
    '#type'          => 'textfield',
    '#title'         => t('Environment'),
    '#description'   => t('Define environment of site.'),
    '#default_value' => $env ? $env : NULL,
    '#maxlength'     => 128,
    '#required'      => TRUE,
  );

  $form['add_class'] = array(
    '#type' => 'checkbox',
    '#title' => t('Use to body class.'),
    '#default_value' => variable_get('token_environment_body_class', NULL),
  );

  $form['save'] = array(
    '#type'     => 'submit',
    '#value'    => t('Save'),
  );

  return $form;
}


/**
 * Submit callback for token_environment_edit_form.
 *
 * @param array $form
 *   The form array.
 * @param array $form_state
 *   The submited form state.
 */
function token_environment_edit_form_submit(array $form, array &$form_state) {
  if ($form_state['values']['environment']) {
    variable_set('token_environment', $form_state['values']['environment']);
    variable_set('token_environment_body_class', $form_state['values']['add_class']);

    $message = t('Your environment <em>@env</em> has been saved.', array('@env' => $form_state['values']['environment']));
    $message .= '<br />';

    if ($form_state['values']['add_class']) {
      $message .= t('And now the environment token is added to body classes.');
    }
    else {
      $message .= t('And now the environment token is removed from body classes.');
    }

    drupal_set_message($message);
  }

  $form_state['redirect'] = 'admin/config/development/token-environment';
}


/**
 * Confirm token environment delete action.
 *
 * @param array $form
 *   The delete token form.
 * @param array $form_state
 *   Form state array.
 *
 * @return array
 *   The complete form.
 */
function token_environment_delete_form(array $form, array &$form_state) {
  // Get environment.
  $env = token_environment_get_env();

  return confirm_form(
    $form,
    t('Are you sure you want to delete environment token <em>@env</em>?', array('@env' => $env)),
    'admin/config/development/token-environment',
    NULL,
    t('Delete'),
    t('Cancel')
  );
}


/**
 * Delete token environment form submit callback.
 *
 * @param array $form
 *   The form array.
 * @param array $form_state
 *   The form state array.
 */
function token_environment_delete_form_submit(array $form, array &$form_state) {
  variable_del('token_environment');
  variable_del('token_environment_body_class');

  drupal_set_message(
    t('Token environment deleted')
  );

  $form_state['redirect'] = 'admin/config/development/token-environment';
}
