CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Usage
 * FAQ
 * Maintainers


INTRODUCTION
------------

The Token Environment module allows you to specify an environment in the
settings.php file and use it how a token. This token can be interesting
if you need create dynamic paths with environment to save files.

 * For a full description of the module, visit the project page:
   https://www.drupal.org/node/2653696/

 * To submit bug reports and feature suggestions, or to track changes:
   https://www.drupal.org/project/issues/2653696


REQUIREMENTS
------------

This module requires the following modules:

 * Token (https://www.drupal.org/project/token)


INSTALLATION
------------

 * Install as you would normally install a contributed Drupal module. See:
   https://drupal.org/documentation/install/modules-themes/modules-7
   for further information.


CONFIGURATION
-------------

Defining environment
 * You can define the environment in settings.php. You can use the environment
   that best suits your needs. To define it you should add
   $conf['env'] = '{environment}' into settings.php.



USAGE
-----

If environment has defined into settings.php you can use it with Token module.
A new Token in Site group will be add, and you can use it with
[site:environment] in any site of your Drupal. To use it you can read
Token help.


FAQ
---

Q: I defined environment in settings.php, but it not working fine.

A: You can check your environment definition in the help page
   "admin/help#token_environment".


MAINTAINERS
-----------

Current maintainers:
 * Guiu Mateu - https://www.drupal.org/u/guiumateu
